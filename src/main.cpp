<<<<<<< HEAD
#include <iostream>
#include <stdio.h>
using namespace std;

#include <sstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include "array.hpp"
#include "crypto.hpp"
#include "network.hpp"
#include "Socket.hpp"


int main(int argc, const char * argv []) {
  Pacote primeiro_passo(0xC0);
  primeiro_passo.bytes();

  byte id[];
  array::array* id_array = array::create(8, id);
  array::array* id_enc = crypto::rsa_encrypt(id_array, key);

  Pacote iniciar_registro = (0xC2, id_enc);
  

	int id;
// 	array::array *cabecalho = array::create(4);
// 	array::array *pacote_enviar = array::create(3);
	
// 	cabecalho->data[0] = 0x03;
// 	cabecalho->data[1] = 0;
// 	cabecalho->data[2] = 0;
// 	cabecalho->data[3] = 0;
	
// 	pacote_enviar->data[0] = 0xC0;
// 	pacote_enviar->data[1] = 0;
// 	pacote_enviar->data[2] = 0;


// 	cout << "Tentando comunicação com o servidor" << endl;
 	if(( id = network::connect("45.55.185.4", 3000)) < 0){
// 	  cout << "Conexão falhou" << endl;
// 	}
// 	else {

RSA *pkey = crypto::rsa_read_private_key_from_PEM("/home/joaoitalo/ep1x/private.pem");
// 	   if(pkey == nullptr || pkey == NULL) {
// 	   	cerr << "Não leu a chave privada." << endl;
// 	   	return 1;
// 	   }

 	   RSA *key = crypto::rsa_read_public_key_from_PEM("/home/joaoitalo/ep1x/server_pk.pem");
 	   if(key == nullptr) {
 	   	cerr << "Não leu chave pública do servidor." << endl;
 	   	return 2;
  }

// 	   network::write( fd, cabecalho);
// 	   network::write( fd, pacote_enviar);
// 	   array::array *pacote_recebe;
// 	   array::array *pacote_cabecalho;
//        pacote_cabecalho = network::read(fd,4);
// 	   pacote_recebe = network::read(fd, 3);
// 	   cout << "Pacote recebido:" << *pacote_recebe << endl;
// 	   byte userID[8]= {0xb3, 0x32, 0x66, 0x7a,  0xa1, 0x00, 0x54, 0xa1};

// 	   array::array* user = array::create(8, userID);
// 	   array::array* idencriptado = crypto::rsa_encrypt(user, key);
// 	   unsigned char bytes[4] = {(idencriptado->length + 23) & 0xff, ((idencriptado->length  + 23) >> 8) & 0xff, ((idencriptado->length  + 23) >> 16) & 0xff, ((idencriptado->length  + 23) >> 24) & 0xff};
// 	    array::array *cab = array::create(4, bytes);

// 	   cout << *cab << endl; 
	 
// 	   array::array *tag_length = array::create(3 + idencriptado->length + 20);
// 	   tag_length->data[0] = 0xC2;
// 	   tag_length->data[1] = idencriptado->length & 0xFF;
// 	   tag_length->data[2] = (idencriptado->length >> 8) & 0xFF;
// 	   memcpy(tag_length->data + 3, idencriptado->data, idencriptado->length);
// 	   memcpy(tag_length->data + 3 + idencriptado->length, crypto::sha1(idencriptado)->data, 20);
// 	    network::write(fd, cab);
// 	    	network::write(fd, tag_length);
// 	   array::array* cabecalho2 =  network::read(fd, 4);
// 	   unsigned int total = 0;
// 	   total |= cabecalho2 -> data[0] ;
// 	   total |= cabecalho2 -> data[1] << 8;
// 	   total |= cabecalho2 -> data[2] << 16;
// 	   total |= cabecalho2 -> data[3] << 24; 

// 	   printf("Lendo os próximos %d bytes\n", total);
// 	   array::array* pacote ;
// 	   pacote = network::read(fd, total);
// 	   cout << "Recebido: " << *pacote << endl;
// 	   total = 0;
// 	   total |= pacote -> data[1];
// 	   total |= pacote -> data[2] << 8;

// 	   cout << "Deu bom " << total << endl;
// 	   array::array* chavenc = array::create(total);
// 	   memcpy(chavenc->data, pacote-> data + 3, total);

// 	   cout << "S encriptada: " << *chavenc << endl;
// 		array::array *dec = crypto::rsa_decrypt(chavenc, pkey);
	   
// 	   if(dec == nullptr) {
// 	   	cerr << "Erro." << endl;
// 	   }
// 	   else {
// 	   	cout << *dec << endl;
// 		}

// 	}
     
 return 0;
=======
#include "crypto.hpp"
#include "array.hpp"
#include "network.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <unistd.h>

using namespace std;

class Packet;

ostream& operator<<(ostream& os, const Packet& p);

class Packet {
public:
	
	Packet(const array::array* data) {
		byte* d = data->data;
		
		this->tag = d[0];
		this->length = d[0] & (d[1] << 8);
		
		if(this->length > 0) {
			this->value = array::create(this->length, d + 3);
			this->signature = array::create(20, d + this->length + 3);
		}
		//TODO verify signature
	}
	
	Packet(const byte tag, const array::array* value)  {
		this->tag = tag;
		this->set_value(value);
	}
	
	Packet(const byte tag) {
		this->tag = tag;
		this->value = nullptr;
		this->signature = nullptr;
	}
	
	~Packet() {
		if(this->value != nullptr) {
			array::destroy(this->value);
			array::destroy(this->signature);
		}
	}
	
	size_t total_size() {
		return 3 + (value != nullptr ? value->length : 0) + (signature != nullptr ? signature->length : 0);
	}
	
	void set_value(const array::array* value) {
	
		if(this->value != nullptr) {
			array::destroy(this->value);
			array::destroy(this->signature);
		}
		
		this->value = array::copy(value);
		if(value != nullptr) {
			this->signature = crypto::sha1(value);	
			this->length = this->value->length;
		}
		else {
			this->length = 0;		
		}
	}
	
	array::array* bytes() {
		array::array* raw;
		
		if(this->length == 0) {
			raw = array::create(3);
		}
		else {
			raw = array::create(this->total_size());
		}
		
		raw->data[0] = this->tag;
		raw->data[1] = this->length & 0xFF;
		raw->data[2] = (this->length >> 8) & 0xFF;
		
		if(this->length > 0) {
			memcpy(raw->data + 3, this->value, this->length);
			memcpy(raw->data + 3 + this->length, this->signature, 20);
		}
		
		return raw;
	}
	
	friend ostream& operator<<(ostream& os, const Packet& p);
private:

	byte tag;
	unsigned short length;
	array::array* value;
	array::array* signature;
};

std::ostream& operator<<(std::ostream& os, const Packet& packet) {
	os << "[tag: " << std::hex << (int) packet.tag << "; " <<
              "len: "  << std::dec << packet.length << ";";
	if(packet.length > 0) {
	      os << " value: {" << *(packet.value) << "}; " <<
	      "sign: {" << *(packet.signature) << "};";
	}
	os << "]";
	
	return os;
}

void sock_send(int sock, Packet* packet) {
	size_t size = packet->total_size();
	array::array* packet_bytes = packet->bytes();

	byte* tmp = new byte[4];
	array::array* header = array::wrap(4, tmp);
	tmp[0] = size & 0xFF;
	tmp[1] = (size >> 8) & 0xFF;
	tmp[2] = (size >> 16) & 0xFF;
	tmp[3] = (size >> 24) & 0xFF;
	
	network::write(sock, header);
	network::write(sock, packet_bytes);
	
	cout << "destroy 1" << endl;
	array::destroy(header);
	cout << "destroy 2" << endl;
	array::destroy(packet_bytes);
	cout << "destroy 3" << endl;
	delete[] tmp;
}

int main(int argc, char** argv) {
	cout << "Connecting...";
	int sock = network::connect("45.55.185.4", 3000);
	
	if(sock < 0) {
		cout << "failed :(" << endl;
		return 1;
	}
	cout << "ok." << endl;
	
	cout << "Sending packets." << endl;
	while(true) {
		cout << ".";
		Packet* packet = new Packet(0xFA, crypto::nonce(32));
		cout << "Packet: " << *packet << endl;
		sock_send(sock, packet);
		cout << "Free" << endl;
		delete packet;
		sleep(3);
	}

	
	return 0;	
>>>>>>> ef880003dfbce9cd89aecddf906603d938c51008
}
